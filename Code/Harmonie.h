#ifndef HARMONIE_H
#define HARMONIE_H

#include <stdio.h>
#include <stdlib.h>
#include <cmath>
#include <cstdlib>
#include <vector>
#include <fstream>
#include <algorithm>
#include <QString>
#include <iostream>

#include <QImage>

using namespace std;

//Structure used to define a RGB value
typedef struct color {
  int R;
  int G;
  int B;
};

//Structure used to define a HSL value
typedef struct HSL {
  int H;
  float S;
  float L;
};

std::vector<color> CercleChromatique(int n);
HSL RGBtoHSL (color couleur);
color HSLtoRGB (HSL couleur);
QImage harmonisation(QString filename,int type, int AngleOuverture);//QString outputName

#endif // HARMONIE_H
