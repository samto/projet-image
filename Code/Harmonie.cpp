
#include "Harmonie.h"
#include "image_ppm.h"
//Function CercleChromatique :
//Input : 
//    - int n : Number of iterations to make (minimum 1, will have 6 default colors)
//Output : 
//    - std::vector<color> : An array of colors from a chromatic circle
std::vector<color> CercleChromatique(int n)
{
  color couleur;


  std::vector<color> liste;

  //Rouge
  couleur.R=255;
  couleur.G=0;
  couleur.B=0;
  liste.push_back(couleur);

  //Orange
  couleur.R=255;
  couleur.G=128;
  couleur.B=0;
  liste.push_back(couleur);


  //Jaune
  couleur.R=255;
  couleur.G=255;
  couleur.B=0;
  liste.push_back(couleur);

  //Vert
  couleur.R=0;
  couleur.G=255;
  couleur.B=0;
  liste.push_back(couleur);

  //Bleu
  couleur.R=0;
  couleur.G=0;
  couleur.B=255;
  liste.push_back(couleur);

  //Violet
  couleur.R=128;
  couleur.G=0;
  couleur.B=255;
  liste.push_back(couleur);

  for (int i=1;i<n;i++)
  {
    int taille = liste.size();
    //faire la moyenne de tous les elements sauf du dernier
    for (int j=0;j<taille*2-2;j=j+2)
    {
      couleur.R=(int)(liste.at(j).R+liste.at(j+1).R)/2;
      couleur.G=(int)(liste.at(j).G+liste.at(j+1).G)/2;
      couleur.B=(int)(liste.at(j).B+liste.at(j+1).B)/2;
      liste.insert(liste.begin()+j+1,couleur);
    }
    //faire le pushback du dernier avec le premier
    couleur.R=(int)(liste.at(0).R+liste.at(liste.size()-1).R)/2;
    couleur.G=(int)(liste.at(0).G+liste.at(liste.size()-1).G)/2;
    couleur.B=(int)(liste.at(0).B+liste.at(liste.size()-1).B)/2;
    liste.push_back(couleur);
  }
  
  return liste;
}

//Function RGBtoHSL :
//Input : 
//    - color couleur : color to convert from the RGB color space to the HSL space
//Output : 
//    - HSL : The HSL value of the input color
HSL RGBtoHSL (color couleur)
{

  HSL result;
  float r,g,b,cmax,cmin,delta;
  r=couleur.R/255.0;
  g=couleur.G/255.0;
  b=couleur.B/255.0;

  cmax=max(max(r,g),b);
  cmin=min(min(r,g),b);

  delta=cmax-cmin;
  if(delta > 0){
    if(cmax == r){
      result.H = 60 * (fmod(((g - b) / delta),6));
    } else if(cmax == g){
      result.H = 60 * ((b - r) / delta + 2);
    } else if(cmax == b){
      result.H = 60 * ((r - g) / delta + 4);
    }

    if(cmax > 0){
      result.S = delta / cmax;
    } else {
      result.S = 0;
    }

    result.L = cmax;
  } else {
    result.H = 0;
    result.S = 0;
    result.L = cmax;
  }

  if(result.H < 0){
    result.H += 360;
  }


  return result;
}

//Function HSLtoRGB :
//Input : 
//    - HSL couleur : color to convert from the HSL color space to the RGB space
//Output : 
//    - color : The RGB value of the input HSL
color HSLtoRGB (HSL couleur)
{
  color result;
  float C, primeH, X, M, R, G, B;
  C = couleur.L * couleur.S;
  primeH = fmod(couleur.H / 60.0, 6);
  X = C * (1 - fabs(fmod(primeH,2) - 1));
  M = couleur.L - C;


  if(0 <= primeH && primeH < 1){
    R = C;
    G = X;
    B = 0;
  } else if(1 <= primeH && primeH < 2){
    R = X;
    G = C;
    B = 0;
  } else if(2 <= primeH && primeH < 3){
    R = 0;
    G = C;
    B = X;
  } else if(3 <= primeH && primeH < 4){
    R = 0;
    G = X;
    B = C;
  } else if(4 <= primeH && primeH < 5){
    R = X;
    G = 0;
    B = C;
  } else if(5 <= primeH && primeH < 6){
    R = C;
    G = 0;
    B = X;
  } else {
    R = 0;
    G = 0;
    B = 0;
  }
  
  result.R = (R + M) * 255;
  result.G = (G + M) * 255;
  result.B = (B + M) * 255;



  return result;
}


QImage harmonisation(QString filename,int type, int AngleOuverture)//QString outputName
{
  //File opening & reading ----------------------------------------------------
  int nH, nW, nTaille;


  //AngleOuverture between 1 and 180

  char *cNomImgLue;// *cNomImgEcrite;

  cNomImgLue = new char[filename.length()+1];
  strcpy(cNomImgLue, filename.toStdString().c_str());

  //cNomImgEcrite = new char[outputName.length()+1];
  //strcpy(cNomImgEcrite, outputName.toStdString().c_str());

  OCTET *ImgIn, *ImgOut,*ImgSeuil,*ImgSeuilCopie;
   
  lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
  nTaille = nH * nW;

  int nTaille3=nTaille*3;
  
  allocation_tableau(ImgIn, OCTET, nTaille3);
  lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
  allocation_tableau(ImgOut, OCTET, nTaille3);
  allocation_tableau(ImgSeuil, OCTET, nTaille);
  allocation_tableau(ImgSeuilCopie, OCTET, nTaille);

  //Definition of the chromatic circle ---------------------------------------
  std::vector<color> listecouleur = CercleChromatique(4);


  //Création du tableau histogramme et initialisation
  int nbcouleur[listecouleur.size()];
  for (int i=0;i<listecouleur.size();i++)
  {
    nbcouleur[i]=0;
  }

  int cptmin=0;
  int diff,diffmin;
  color pixel,test;
  HSL pixel2,test2;
  

  //Fill the histogram with closest value in the chromatic circle
  for (int i=0;i<nTaille;i++)
  {
    diffmin=1000;


    pixel.R=ImgIn[i*3];
    pixel.G=ImgIn[i*3+1];
    pixel.B=ImgIn[i*3+2];
    pixel2=RGBtoHSL(pixel);


    for (int j=0;j<listecouleur.size();j++)
    {

      test=listecouleur.at(j);
      test2=RGBtoHSL(test);


      diff=abs(pixel2.H-test2.H);
      if (diff<diffmin)
      {
        cptmin=j;
        diffmin=diff;
      }
    }
    
    nbcouleur[cptmin]+=1;

  }

  //The selected color becomes the dominant color of the image
   //The selected color becomes the both dominant color of the image
  vector<int> tableautrie;
  for (int i=0;i<listecouleur.size();i++)
  {
    tableautrie.push_back(nbcouleur[i]);
  }

  std::sort(tableautrie.begin(),tableautrie.end());

  int valmax1,valmax2,valmax3;
  int cptmax1,cptmax2,cptmax3;

  valmax1=tableautrie.at(tableautrie.size()-1);

  for (int i=0;i<listecouleur.size();i++)
  {
    if (nbcouleur[i]==valmax1)
    {
      cptmax1=i;
    }
  }
  int var=2;

  do{
    valmax2=tableautrie.at(tableautrie.size()-var);
    for (int i=0;i<listecouleur.size();i++)
  {
    if (nbcouleur[i]==valmax2)
    {
      cptmax2=i;
    }

  }
  var++;

  }
  while(abs(cptmax1-cptmax2)<tableautrie.size()/5);


  //Hue histogram
  int histoHue[360]= {0};



  //Selection of complementary colors ----------------------------------------
  int choix;
  int choix2;
  int complementaire1;
  int complementaire2;
  

  //Vector containing all needed hues for the image modification
  std::vector<HSL> huePalette;


  if(type==1)//complémentaire
  {
    //Dominant color and its complementary
    choix = cptmax1;
    complementaire1=(choix+listecouleur.size()/2)%listecouleur.size();

    //Push back the selected color converted into HSL
    huePalette.push_back(RGBtoHSL(listecouleur.at(choix)));

    //Push back the complementary color converted into HSL
    huePalette.push_back(RGBtoHSL(listecouleur.at(complementaire1)));

  }
  else if(type==2)//complémentaire Adjacente
  {
    choix = cptmax1;
    complementaire1=(choix+listecouleur.size()/2+1)%listecouleur.size();
    complementaire2=(choix+listecouleur.size()/2-1)%listecouleur.size();


    //Push back the selected color converted into HSL
    huePalette.push_back(RGBtoHSL(listecouleur.at(choix)));

    //Push back the complementary color converted into HSL
    huePalette.push_back(RGBtoHSL(listecouleur.at(complementaire1)));
    huePalette.push_back(RGBtoHSL(listecouleur.at(complementaire2)));

  }
  else if(type==3)//Tétradique
  {
    choix = cptmax1;
    choix2 = cptmax2;


    complementaire1=(choix+listecouleur.size()/2)%listecouleur.size();
    complementaire2=(choix2+listecouleur.size()/2)%listecouleur.size();
    
    //Push back the selected color converted into HSL
    huePalette.push_back(RGBtoHSL(listecouleur.at(choix)));
    huePalette.push_back(RGBtoHSL(listecouleur.at(choix2)));

    //Push back the complementary color converted into HSL
    huePalette.push_back(RGBtoHSL(listecouleur.at(complementaire1)));
    huePalette.push_back(RGBtoHSL(listecouleur.at(complementaire2)));

  } 
  else if(type==4)//Triadique
  {
    choix = cptmax1;
    
    complementaire1=(choix+listecouleur.size()/3)%listecouleur.size();
    complementaire2=(choix-listecouleur.size()/3)%listecouleur.size();

    //Push back the selected color converted into HSL
    huePalette.push_back(RGBtoHSL(listecouleur.at(choix)));

    //Push back the complementary color converted into HSL
    huePalette.push_back(RGBtoHSL(listecouleur.at(complementaire1)));
    huePalette.push_back(RGBtoHSL(listecouleur.at(complementaire2)));

  }
  else if(type==5)//Mono
  {
    
    choix = cptmax1;
    
    //Push back the selected color converted into HSL
    huePalette.push_back(RGBtoHSL(listecouleur.at(choix)));
    AngleOuverture=0;

  }
  else
  {
    choix = cptmax1;

    complementaire1=(choix+1)%listecouleur.size();
    complementaire2=(choix+listecouleur.size()-1)%listecouleur.size();
    
    //Push back the selected color converted into HSL
    huePalette.push_back(RGBtoHSL(listecouleur.at(choix)));

    //Push back the complementary color converted into HSL
    huePalette.push_back(RGBtoHSL(listecouleur.at(complementaire1)));
    huePalette.push_back(RGBtoHSL(listecouleur.at(complementaire2)));
  }

  





  
  

  
  color couleur;
  HSL HUE;
  int cpt;
  int ite=0;
  int nbMaxVoisins=5;
  bool changement;

  //Loop through each pixel
  for (int i=0;i<nTaille;i++)
  {
    diffmin=1000;

    couleur.R=ImgIn[i*3];
    couleur.G=ImgIn[i*3+1];
    couleur.B=ImgIn[i*3+2];
    HUE = RGBtoHSL(couleur);

    //We loop through each hue in the palette and get the closest one to the pixel we're reading
    for (int j=0;j<huePalette.size();j++)
    {
      //Since the hue value is in degrees and 0 & 360 correspond to the same value we compute a distance accordingly
      diff=min(abs(huePalette.at(j).H-HUE.H),abs(huePalette.at(j).H-HUE.H+360));

      //If the distance is the lowest we store the index of the hue for later
      if (diff<diffmin)
      {
        diffmin=diff;
        cptmin=j;
      }
    }


    ImgSeuil[i]=cptmin;

  }

  //Check to see if a pixel possess enough neighbors that are different enough from a threshold to modify the current pixel & limit artifacts
  //Loop with recursion if a value has been changed
  ImgSeuilCopie=ImgSeuil;
  do
    {
      
      changement=false;
      ite++;
      ImgSeuil=ImgSeuilCopie;
      for (int i=0;i<nTaille;i++)
      {
        

        if(i>=nW || i<=nTaille-nW || i%nW!=0 || i%nW!=nW-1)
        {
          for (int j=0;j<huePalette.size();j++)
          {
            cpt=0;
            if (ImgSeuil[i-nW-1]!=ImgSeuil[i] && ImgSeuil[i-nW-1]==j)
            {
              cpt++;
            }
            if (ImgSeuil[i-nW]!=ImgSeuil[i] && ImgSeuil[i-nW]==j)
            {
              cpt++;
            }
            if (ImgSeuil[i-nW+1]!=ImgSeuil[i] && ImgSeuil[i-nW+1]==j)
            {
              cpt++;
            }
            if (ImgSeuil[i-1]!=ImgSeuil[i] && ImgSeuil[i-1]==j)
            {
              cpt++;
            }
            if (ImgSeuil[i+1]!=ImgSeuil[i] && ImgSeuil[i+1]==j)
            {
              cpt++;
            }
            if (ImgSeuil[i+nW-1]!=ImgSeuil[i] && ImgSeuil[i+nW-1]==j)
            {
              cpt++;
            }
            if (ImgSeuil[i+nW]!=ImgSeuil[i] && ImgSeuil[i+nW-1]==j)
            {
             cpt++;
            }
            if (ImgSeuil[i+nW+1]!=ImgSeuil[i] && ImgSeuil[i+nW-1]==j)
            {
             cpt++;
            }
            if (cpt>= nbMaxVoisins)
            {
             ImgSeuilCopie[i]=j;
             changement=true;
            }
          }

        }
          
      }
    }
    while(changement);



    for (int i=0;i<nTaille;i++)
    {
      couleur.R=ImgIn[i*3];
      couleur.G=ImgIn[i*3+1];
      couleur.B=ImgIn[i*3+2];
      HUE = RGBtoHSL(couleur);




    //choix de la facon pour atteindre le but choisi
    if (huePalette.at(ImgSeuilCopie[i]).H > HUE.H)
    {
      if (huePalette.at(ImgSeuilCopie[i]).H - HUE.H > 180)
      {
        HUE.H = (360+(huePalette.at(ImgSeuilCopie[i]).H + (360 - huePalette.at(ImgSeuilCopie[i]).H + HUE.H)*AngleOuverture/180))%360;

      }
      else
      {
        HUE.H = (360+(huePalette.at(ImgSeuilCopie[i]).H - (abs(huePalette.at(ImgSeuilCopie[i]).H-HUE.H)*AngleOuverture/180)))%360;

      }
    }
    else
    {
      if (HUE.H - huePalette.at(ImgSeuilCopie[i]).H > 180)
      {
        HUE.H = (360+(huePalette.at(ImgSeuilCopie[i]).H - (360 - HUE.H + huePalette.at(ImgSeuilCopie[i]).H)*AngleOuverture/180))%360;
      }
      else
      {
        HUE.H = (360+(huePalette.at(ImgSeuilCopie[i]).H + (abs(HUE.H-huePalette.at(ImgSeuilCopie[i]).H)*AngleOuverture/180)))%360;

      }
    }

    histoHue[HUE.H]++;




    //We retrieve the hue from the palette with the closest color
    // HUE.H = huePalette.at(cptmin).H;
    
    //We convert the pixel back to RGB
    couleur=HSLtoRGB(HUE);

    //We write the returned color into the picture
    ImgOut[i*3]=couleur.R;
    ImgOut[i*3+1]=couleur.G;
    ImgOut[i*3+2]=couleur.B;
  }
  
  QImage imagefinale;
  imagefinale=QImage(ImgOut,nW,nH,QImage::Format_RGB888);

  
  //ecrire_image_ppm(cNomImgEcrite, ImgOut,  nH, nW);
  return imagefinale;
}
