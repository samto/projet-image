#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "Harmonie.h"
#include <iostream>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_btn_image_clicked()
{
    filename = QFileDialog::getOpenFileName(this,tr("Choose"),"",tr("Images (*.ppm)"));
    if(QString::compare(filename, QString()) != 0)
    {
        QImage image;
        bool valid = image.load(filename);

        if (valid)
        {
            image=image.scaledToWidth(ui->lbl_image->width(),Qt::SmoothTransformation);
            image=image.scaledToHeight(ui->lbl_image->height(),Qt::SmoothTransformation);
            ui->lbl_image->setPixmap(QPixmap::fromImage(image));
        }

    }
}

void MainWindow::on_horizontalSlider_valueChanged(int value)
{
    ui->lbl_slider_value->setText(QString::number(value) + "°");
    angleOuverture = value;
}

void MainWindow::on_horizontalSlider_sliderReleased()
{
    onUpdate();
}


void MainWindow::onUpdate(){
    if(QFile::exists(filename)){
        imagefinale= harmonisation(filename,type,angleOuverture);

        imagefinale=imagefinale.scaledToWidth(ui->lbl_image_2->width(),Qt::SmoothTransformation);
        imagefinale=imagefinale.scaledToHeight(ui->lbl_image_2->height(),Qt::SmoothTransformation);
        ui->lbl_image_2->setPixmap(QPixmap::fromImage(imagefinale));
    }
}

void MainWindow::on_btn_execute_clicked()
{

    int angleOuverture = ui->lbl_slider_value->text().toInt();
    QString newname = filename.left(filename.size()-4) + methode + QString::number(angleOuverture) +".ppm";
    imagefinale= harmonisation(filename,type,angleOuverture);
    imagefinale.save(newname,0);


    return;
}

void MainWindow::on_radioButton_1_clicked()
{
    type=1;
    methode="Comp";
    onUpdate();
}

void MainWindow::on_radioButton_2_clicked()
{
    type=2;
    methode="CompAdj";
    onUpdate();
}

void MainWindow::on_radioButton_3_clicked()
{
    type=3;
    methode="Teta";
    onUpdate();
}

void MainWindow::on_radioButton_4_clicked()
{
    type=4;
    methode="Tria";
    onUpdate();
}

void MainWindow::on_radioButton_5_clicked()
{
    type=5;
    methode="Mono";
    onUpdate();
}

void MainWindow::on_radioButton_6_clicked()
{
    type=6;
    methode="Analogue";
    onUpdate();
}


