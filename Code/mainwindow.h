#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_btn_image_clicked();

    void on_horizontalSlider_valueChanged(int value);

    void on_btn_execute_clicked();

    void onUpdate();


    void on_radioButton_1_clicked();

    void on_radioButton_2_clicked();

    void on_radioButton_3_clicked();

    void on_radioButton_4_clicked();

    void on_radioButton_5_clicked();

    void on_radioButton_6_clicked();

    void on_horizontalSlider_sliderReleased();

private:
    QString filename=QString();
    Ui::MainWindow *ui;
    QImage imagefinale=QImage();
    int type=1;
    int angleOuverture = 0;
    QString methode="Comp";
};
#endif // MAINWINDOW_H
