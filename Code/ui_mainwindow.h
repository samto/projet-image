/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.8
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QPushButton *btn_image;
    QLabel *lbl_image;
    QSlider *horizontalSlider;
    QLabel *lbl_slider_text;
    QLabel *lbl_slider_value;
    QRadioButton *radioButton_1;
    QRadioButton *radioButton_2;
    QRadioButton *radioButton_3;
    QRadioButton *radioButton_4;
    QRadioButton *radioButton_5;
    QRadioButton *radioButton_6;
    QPushButton *btn_execute;
    QLabel *lbl_image_2;
    QLabel *lbl_title1;
    QLabel *lbl_title2;
    QLabel *lbl_background_input;
    QLabel *lbl_slider_text_2;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(1224, 952);
        MainWindow->setMinimumSize(QSize(1224, 952));
        MainWindow->setMaximumSize(QSize(1224, 952));
        QPalette palette;
        QBrush brush(QColor(255, 255, 255, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Base, brush);
        QBrush brush1(QColor(136, 138, 133, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        MainWindow->setPalette(palette);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        btn_image = new QPushButton(centralwidget);
        btn_image->setObjectName(QString::fromUtf8("btn_image"));
        btn_image->setGeometry(QRect(960, 70, 181, 61));
        QPalette palette1;
        palette1.setBrush(QPalette::Active, QPalette::WindowText, brush);
        QBrush brush2(QColor(85, 87, 83, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Button, brush2);
        QBrush brush3(QColor(127, 131, 124, 255));
        brush3.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Light, brush3);
        QBrush brush4(QColor(106, 109, 103, 255));
        brush4.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Midlight, brush4);
        QBrush brush5(QColor(42, 43, 41, 255));
        brush5.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Dark, brush5);
        QBrush brush6(QColor(56, 58, 55, 255));
        brush6.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Mid, brush6);
        palette1.setBrush(QPalette::Active, QPalette::Text, brush);
        palette1.setBrush(QPalette::Active, QPalette::BrightText, brush);
        palette1.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        QBrush brush7(QColor(0, 0, 0, 255));
        brush7.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Base, brush7);
        palette1.setBrush(QPalette::Active, QPalette::Window, brush2);
        palette1.setBrush(QPalette::Active, QPalette::Shadow, brush7);
        palette1.setBrush(QPalette::Active, QPalette::AlternateBase, brush5);
        QBrush brush8(QColor(255, 255, 220, 255));
        brush8.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::ToolTipBase, brush8);
        palette1.setBrush(QPalette::Active, QPalette::ToolTipText, brush7);
        QBrush brush9(QColor(255, 255, 255, 128));
        brush9.setStyle(Qt::SolidPattern);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette1.setBrush(QPalette::Active, QPalette::PlaceholderText, brush9);
#endif
        palette1.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::Button, brush2);
        palette1.setBrush(QPalette::Inactive, QPalette::Light, brush3);
        palette1.setBrush(QPalette::Inactive, QPalette::Midlight, brush4);
        palette1.setBrush(QPalette::Inactive, QPalette::Dark, brush5);
        palette1.setBrush(QPalette::Inactive, QPalette::Mid, brush6);
        palette1.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::BrightText, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::Base, brush7);
        palette1.setBrush(QPalette::Inactive, QPalette::Window, brush2);
        palette1.setBrush(QPalette::Inactive, QPalette::Shadow, brush7);
        palette1.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush5);
        palette1.setBrush(QPalette::Inactive, QPalette::ToolTipBase, brush8);
        palette1.setBrush(QPalette::Inactive, QPalette::ToolTipText, brush7);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette1.setBrush(QPalette::Inactive, QPalette::PlaceholderText, brush9);
#endif
        palette1.setBrush(QPalette::Disabled, QPalette::WindowText, brush5);
        palette1.setBrush(QPalette::Disabled, QPalette::Button, brush2);
        palette1.setBrush(QPalette::Disabled, QPalette::Light, brush3);
        palette1.setBrush(QPalette::Disabled, QPalette::Midlight, brush4);
        palette1.setBrush(QPalette::Disabled, QPalette::Dark, brush5);
        palette1.setBrush(QPalette::Disabled, QPalette::Mid, brush6);
        palette1.setBrush(QPalette::Disabled, QPalette::Text, brush5);
        palette1.setBrush(QPalette::Disabled, QPalette::BrightText, brush);
        palette1.setBrush(QPalette::Disabled, QPalette::ButtonText, brush5);
        palette1.setBrush(QPalette::Disabled, QPalette::Base, brush2);
        palette1.setBrush(QPalette::Disabled, QPalette::Window, brush2);
        palette1.setBrush(QPalette::Disabled, QPalette::Shadow, brush7);
        palette1.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush2);
        palette1.setBrush(QPalette::Disabled, QPalette::ToolTipBase, brush8);
        palette1.setBrush(QPalette::Disabled, QPalette::ToolTipText, brush7);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette1.setBrush(QPalette::Disabled, QPalette::PlaceholderText, brush9);
#endif
        btn_image->setPalette(palette1);
        lbl_image = new QLabel(centralwidget);
        lbl_image->setObjectName(QString::fromUtf8("lbl_image"));
        lbl_image->setGeometry(QRect(65, 40, 711, 391));
        QPalette palette2;
        palette2.setBrush(QPalette::Active, QPalette::Base, brush);
        QBrush brush10(QColor(238, 238, 236, 255));
        brush10.setStyle(Qt::SolidPattern);
        palette2.setBrush(QPalette::Active, QPalette::Window, brush10);
        QBrush brush11(QColor(239, 41, 41, 255));
        brush11.setStyle(Qt::SolidPattern);
        palette2.setBrush(QPalette::Active, QPalette::LinkVisited, brush11);
        palette2.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette2.setBrush(QPalette::Inactive, QPalette::Window, brush10);
        palette2.setBrush(QPalette::Inactive, QPalette::LinkVisited, brush11);
        palette2.setBrush(QPalette::Disabled, QPalette::Base, brush10);
        palette2.setBrush(QPalette::Disabled, QPalette::Window, brush10);
        palette2.setBrush(QPalette::Disabled, QPalette::LinkVisited, brush11);
        lbl_image->setPalette(palette2);
        lbl_image->setAutoFillBackground(true);
        lbl_image->setStyleSheet(QString::fromUtf8(""));
        lbl_image->setAlignment(Qt::AlignCenter);
        horizontalSlider = new QSlider(centralwidget);
        horizontalSlider->setObjectName(QString::fromUtf8("horizontalSlider"));
        horizontalSlider->setGeometry(QRect(920, 490, 291, 131));
        QPalette palette3;
        QBrush brush12(QColor(211, 215, 207, 255));
        brush12.setStyle(Qt::SolidPattern);
        palette3.setBrush(QPalette::Active, QPalette::WindowText, brush12);
        palette3.setBrush(QPalette::Active, QPalette::Button, brush1);
        palette3.setBrush(QPalette::Active, QPalette::ButtonText, brush12);
        QBrush brush13(QColor(46, 52, 54, 255));
        brush13.setStyle(Qt::SolidPattern);
        palette3.setBrush(QPalette::Active, QPalette::Base, brush13);
        palette3.setBrush(QPalette::Inactive, QPalette::WindowText, brush12);
        palette3.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette3.setBrush(QPalette::Inactive, QPalette::ButtonText, brush12);
        palette3.setBrush(QPalette::Inactive, QPalette::Base, brush13);
        QBrush brush14(QColor(190, 190, 190, 255));
        brush14.setStyle(Qt::SolidPattern);
        palette3.setBrush(QPalette::Disabled, QPalette::WindowText, brush14);
        palette3.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette3.setBrush(QPalette::Disabled, QPalette::ButtonText, brush14);
        palette3.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        horizontalSlider->setPalette(palette3);
        horizontalSlider->setMaximum(180);
        horizontalSlider->setPageStep(20);
        horizontalSlider->setTracking(true);
        horizontalSlider->setOrientation(Qt::Horizontal);
        horizontalSlider->setInvertedAppearance(false);
        lbl_slider_text = new QLabel(centralwidget);
        lbl_slider_text->setObjectName(QString::fromUtf8("lbl_slider_text"));
        lbl_slider_text->setGeometry(QRect(920, 500, 120, 40));
        QPalette palette4;
        QBrush brush15(QColor(243, 243, 243, 255));
        brush15.setStyle(Qt::SolidPattern);
        palette4.setBrush(QPalette::Active, QPalette::WindowText, brush15);
        palette4.setBrush(QPalette::Inactive, QPalette::WindowText, brush15);
        palette4.setBrush(QPalette::Disabled, QPalette::WindowText, brush14);
        lbl_slider_text->setPalette(palette4);
        lbl_slider_value = new QLabel(centralwidget);
        lbl_slider_value->setObjectName(QString::fromUtf8("lbl_slider_value"));
        lbl_slider_value->setGeometry(QRect(1030, 500, 120, 40));
        QPalette palette5;
        palette5.setBrush(QPalette::Active, QPalette::WindowText, brush15);
        palette5.setBrush(QPalette::Inactive, QPalette::WindowText, brush15);
        palette5.setBrush(QPalette::Disabled, QPalette::WindowText, brush14);
        lbl_slider_value->setPalette(palette5);
        radioButton_1 = new QRadioButton(centralwidget);
        radioButton_1->setObjectName(QString::fromUtf8("radioButton_1"));
        radioButton_1->setGeometry(QRect(920, 220, 121, 31));
        QPalette palette6;
        palette6.setBrush(QPalette::Active, QPalette::WindowText, brush15);
        palette6.setBrush(QPalette::Active, QPalette::Button, brush);
        palette6.setBrush(QPalette::Active, QPalette::Text, brush);
        palette6.setBrush(QPalette::Active, QPalette::ButtonText, brush13);
        palette6.setBrush(QPalette::Active, QPalette::Base, brush13);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette6.setBrush(QPalette::Active, QPalette::PlaceholderText, brush9);
#endif
        palette6.setBrush(QPalette::Inactive, QPalette::WindowText, brush15);
        palette6.setBrush(QPalette::Inactive, QPalette::Button, brush);
        palette6.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette6.setBrush(QPalette::Inactive, QPalette::ButtonText, brush13);
        palette6.setBrush(QPalette::Inactive, QPalette::Base, brush13);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette6.setBrush(QPalette::Inactive, QPalette::PlaceholderText, brush9);
#endif
        palette6.setBrush(QPalette::Disabled, QPalette::WindowText, brush14);
        palette6.setBrush(QPalette::Disabled, QPalette::Button, brush);
        palette6.setBrush(QPalette::Disabled, QPalette::Text, brush14);
        palette6.setBrush(QPalette::Disabled, QPalette::ButtonText, brush14);
        palette6.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        QBrush brush16(QColor(0, 0, 0, 128));
        brush16.setStyle(Qt::SolidPattern);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette6.setBrush(QPalette::Disabled, QPalette::PlaceholderText, brush16);
#endif
        radioButton_1->setPalette(palette6);
        radioButton_1->setChecked(true);
        radioButton_2 = new QRadioButton(centralwidget);
        radioButton_2->setObjectName(QString::fromUtf8("radioButton_2"));
        radioButton_2->setGeometry(QRect(920, 250, 181, 31));
        QPalette palette7;
        palette7.setBrush(QPalette::Active, QPalette::WindowText, brush15);
        palette7.setBrush(QPalette::Active, QPalette::Button, brush);
        palette7.setBrush(QPalette::Active, QPalette::Text, brush);
        palette7.setBrush(QPalette::Active, QPalette::ButtonText, brush13);
        palette7.setBrush(QPalette::Active, QPalette::Base, brush13);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette7.setBrush(QPalette::Active, QPalette::PlaceholderText, brush9);
#endif
        palette7.setBrush(QPalette::Inactive, QPalette::WindowText, brush15);
        palette7.setBrush(QPalette::Inactive, QPalette::Button, brush);
        palette7.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette7.setBrush(QPalette::Inactive, QPalette::ButtonText, brush13);
        palette7.setBrush(QPalette::Inactive, QPalette::Base, brush13);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette7.setBrush(QPalette::Inactive, QPalette::PlaceholderText, brush9);
#endif
        palette7.setBrush(QPalette::Disabled, QPalette::WindowText, brush14);
        palette7.setBrush(QPalette::Disabled, QPalette::Button, brush);
        palette7.setBrush(QPalette::Disabled, QPalette::Text, brush14);
        palette7.setBrush(QPalette::Disabled, QPalette::ButtonText, brush14);
        palette7.setBrush(QPalette::Disabled, QPalette::Base, brush1);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette7.setBrush(QPalette::Disabled, QPalette::PlaceholderText, brush16);
#endif
        radioButton_2->setPalette(palette7);
        radioButton_3 = new QRadioButton(centralwidget);
        radioButton_3->setObjectName(QString::fromUtf8("radioButton_3"));
        radioButton_3->setGeometry(QRect(920, 280, 96, 31));
        QPalette palette8;
        palette8.setBrush(QPalette::Active, QPalette::WindowText, brush15);
        palette8.setBrush(QPalette::Active, QPalette::Button, brush);
        palette8.setBrush(QPalette::Active, QPalette::Text, brush);
        palette8.setBrush(QPalette::Active, QPalette::ButtonText, brush13);
        palette8.setBrush(QPalette::Active, QPalette::Base, brush13);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette8.setBrush(QPalette::Active, QPalette::PlaceholderText, brush9);
#endif
        palette8.setBrush(QPalette::Inactive, QPalette::WindowText, brush15);
        palette8.setBrush(QPalette::Inactive, QPalette::Button, brush);
        palette8.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette8.setBrush(QPalette::Inactive, QPalette::ButtonText, brush13);
        palette8.setBrush(QPalette::Inactive, QPalette::Base, brush13);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette8.setBrush(QPalette::Inactive, QPalette::PlaceholderText, brush9);
#endif
        palette8.setBrush(QPalette::Disabled, QPalette::WindowText, brush14);
        palette8.setBrush(QPalette::Disabled, QPalette::Button, brush);
        palette8.setBrush(QPalette::Disabled, QPalette::Text, brush14);
        palette8.setBrush(QPalette::Disabled, QPalette::ButtonText, brush14);
        palette8.setBrush(QPalette::Disabled, QPalette::Base, brush1);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette8.setBrush(QPalette::Disabled, QPalette::PlaceholderText, brush16);
#endif
        radioButton_3->setPalette(palette8);
        radioButton_4 = new QRadioButton(centralwidget);
        radioButton_4->setObjectName(QString::fromUtf8("radioButton_4"));
        radioButton_4->setGeometry(QRect(920, 310, 96, 31));
        QPalette palette9;
        palette9.setBrush(QPalette::Active, QPalette::WindowText, brush15);
        palette9.setBrush(QPalette::Active, QPalette::Button, brush);
        palette9.setBrush(QPalette::Active, QPalette::Text, brush);
        palette9.setBrush(QPalette::Active, QPalette::ButtonText, brush13);
        palette9.setBrush(QPalette::Active, QPalette::Base, brush13);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette9.setBrush(QPalette::Active, QPalette::PlaceholderText, brush9);
#endif
        palette9.setBrush(QPalette::Inactive, QPalette::WindowText, brush15);
        palette9.setBrush(QPalette::Inactive, QPalette::Button, brush);
        palette9.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette9.setBrush(QPalette::Inactive, QPalette::ButtonText, brush13);
        palette9.setBrush(QPalette::Inactive, QPalette::Base, brush13);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette9.setBrush(QPalette::Inactive, QPalette::PlaceholderText, brush9);
#endif
        palette9.setBrush(QPalette::Disabled, QPalette::WindowText, brush14);
        palette9.setBrush(QPalette::Disabled, QPalette::Button, brush);
        palette9.setBrush(QPalette::Disabled, QPalette::Text, brush14);
        palette9.setBrush(QPalette::Disabled, QPalette::ButtonText, brush14);
        palette9.setBrush(QPalette::Disabled, QPalette::Base, brush1);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette9.setBrush(QPalette::Disabled, QPalette::PlaceholderText, brush16);
#endif
        radioButton_4->setPalette(palette9);
        radioButton_5 = new QRadioButton(centralwidget);
        radioButton_5->setObjectName(QString::fromUtf8("radioButton_5"));
        radioButton_5->setGeometry(QRect(920, 340, 96, 31));
        QPalette palette10;
        palette10.setBrush(QPalette::Active, QPalette::WindowText, brush15);
        palette10.setBrush(QPalette::Active, QPalette::Button, brush);
        palette10.setBrush(QPalette::Active, QPalette::Text, brush);
        palette10.setBrush(QPalette::Active, QPalette::ButtonText, brush13);
        palette10.setBrush(QPalette::Active, QPalette::Base, brush13);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette10.setBrush(QPalette::Active, QPalette::PlaceholderText, brush9);
#endif
        palette10.setBrush(QPalette::Inactive, QPalette::WindowText, brush15);
        palette10.setBrush(QPalette::Inactive, QPalette::Button, brush);
        palette10.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette10.setBrush(QPalette::Inactive, QPalette::ButtonText, brush13);
        palette10.setBrush(QPalette::Inactive, QPalette::Base, brush13);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette10.setBrush(QPalette::Inactive, QPalette::PlaceholderText, brush9);
#endif
        palette10.setBrush(QPalette::Disabled, QPalette::WindowText, brush14);
        palette10.setBrush(QPalette::Disabled, QPalette::Button, brush);
        palette10.setBrush(QPalette::Disabled, QPalette::Text, brush14);
        palette10.setBrush(QPalette::Disabled, QPalette::ButtonText, brush14);
        palette10.setBrush(QPalette::Disabled, QPalette::Base, brush1);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette10.setBrush(QPalette::Disabled, QPalette::PlaceholderText, brush16);
#endif
        radioButton_5->setPalette(palette10);
        radioButton_6 = new QRadioButton(centralwidget);
        radioButton_6->setObjectName(QString::fromUtf8("radioButton_6"));
        radioButton_6->setGeometry(QRect(920, 370, 96, 31));
        QPalette palette11;
        palette11.setBrush(QPalette::Active, QPalette::WindowText, brush15);
        palette11.setBrush(QPalette::Active, QPalette::Button, brush);
        palette11.setBrush(QPalette::Active, QPalette::Text, brush);
        palette11.setBrush(QPalette::Active, QPalette::ButtonText, brush13);
        palette11.setBrush(QPalette::Active, QPalette::Base, brush13);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette11.setBrush(QPalette::Active, QPalette::PlaceholderText, brush9);
#endif
        palette11.setBrush(QPalette::Inactive, QPalette::WindowText, brush15);
        palette11.setBrush(QPalette::Inactive, QPalette::Button, brush);
        palette11.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette11.setBrush(QPalette::Inactive, QPalette::ButtonText, brush13);
        palette11.setBrush(QPalette::Inactive, QPalette::Base, brush13);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette11.setBrush(QPalette::Inactive, QPalette::PlaceholderText, brush9);
#endif
        palette11.setBrush(QPalette::Disabled, QPalette::WindowText, brush14);
        palette11.setBrush(QPalette::Disabled, QPalette::Button, brush);
        palette11.setBrush(QPalette::Disabled, QPalette::Text, brush14);
        palette11.setBrush(QPalette::Disabled, QPalette::ButtonText, brush14);
        palette11.setBrush(QPalette::Disabled, QPalette::Base, brush1);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette11.setBrush(QPalette::Disabled, QPalette::PlaceholderText, brush16);
#endif
        radioButton_6->setPalette(palette11);
        btn_execute = new QPushButton(centralwidget);
        btn_execute->setObjectName(QString::fromUtf8("btn_execute"));
        btn_execute->setGeometry(QRect(960, 750, 171, 61));
        QPalette palette12;
        palette12.setBrush(QPalette::Active, QPalette::WindowText, brush15);
        palette12.setBrush(QPalette::Active, QPalette::Button, brush2);
        palette12.setBrush(QPalette::Active, QPalette::Text, brush15);
        palette12.setBrush(QPalette::Active, QPalette::ButtonText, brush15);
        palette12.setBrush(QPalette::Active, QPalette::Base, brush2);
        QBrush brush17(QColor(243, 243, 243, 128));
        brush17.setStyle(Qt::SolidPattern);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette12.setBrush(QPalette::Active, QPalette::PlaceholderText, brush17);
#endif
        palette12.setBrush(QPalette::Inactive, QPalette::WindowText, brush15);
        palette12.setBrush(QPalette::Inactive, QPalette::Button, brush2);
        palette12.setBrush(QPalette::Inactive, QPalette::Text, brush15);
        palette12.setBrush(QPalette::Inactive, QPalette::ButtonText, brush15);
        palette12.setBrush(QPalette::Inactive, QPalette::Base, brush2);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette12.setBrush(QPalette::Inactive, QPalette::PlaceholderText, brush17);
#endif
        palette12.setBrush(QPalette::Disabled, QPalette::WindowText, brush14);
        palette12.setBrush(QPalette::Disabled, QPalette::Button, brush2);
        palette12.setBrush(QPalette::Disabled, QPalette::Text, brush14);
        palette12.setBrush(QPalette::Disabled, QPalette::ButtonText, brush14);
        palette12.setBrush(QPalette::Disabled, QPalette::Base, brush1);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette12.setBrush(QPalette::Disabled, QPalette::PlaceholderText, brush16);
#endif
        btn_execute->setPalette(palette12);
        lbl_image_2 = new QLabel(centralwidget);
        lbl_image_2->setObjectName(QString::fromUtf8("lbl_image_2"));
        lbl_image_2->setGeometry(QRect(65, 470, 711, 391));
        QPalette palette13;
        palette13.setBrush(QPalette::Active, QPalette::Base, brush);
        palette13.setBrush(QPalette::Active, QPalette::Window, brush10);
        palette13.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette13.setBrush(QPalette::Inactive, QPalette::Window, brush10);
        palette13.setBrush(QPalette::Disabled, QPalette::Base, brush10);
        palette13.setBrush(QPalette::Disabled, QPalette::Window, brush10);
        lbl_image_2->setPalette(palette13);
        lbl_image_2->setAutoFillBackground(true);
        lbl_image_2->setAlignment(Qt::AlignCenter);
        lbl_title1 = new QLabel(centralwidget);
        lbl_title1->setObjectName(QString::fromUtf8("lbl_title1"));
        lbl_title1->setGeometry(QRect(50, 20, 150, 40));
        QPalette palette14;
        palette14.setBrush(QPalette::Active, QPalette::WindowText, brush15);
        palette14.setBrush(QPalette::Active, QPalette::Text, brush);
        palette14.setBrush(QPalette::Active, QPalette::Base, brush);
        palette14.setBrush(QPalette::Active, QPalette::Window, brush2);
        QBrush brush18(QColor(186, 189, 182, 128));
        brush18.setStyle(Qt::SolidPattern);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette14.setBrush(QPalette::Active, QPalette::PlaceholderText, brush18);
#endif
        palette14.setBrush(QPalette::Inactive, QPalette::WindowText, brush15);
        palette14.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette14.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette14.setBrush(QPalette::Inactive, QPalette::Window, brush2);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette14.setBrush(QPalette::Inactive, QPalette::PlaceholderText, brush18);
#endif
        palette14.setBrush(QPalette::Disabled, QPalette::WindowText, brush14);
        palette14.setBrush(QPalette::Disabled, QPalette::Text, brush14);
        palette14.setBrush(QPalette::Disabled, QPalette::Base, brush2);
        palette14.setBrush(QPalette::Disabled, QPalette::Window, brush2);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette14.setBrush(QPalette::Disabled, QPalette::PlaceholderText, brush16);
#endif
        lbl_title1->setPalette(palette14);
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        lbl_title1->setFont(font);
        lbl_title1->setAutoFillBackground(true);
        lbl_title1->setAlignment(Qt::AlignCenter);
        lbl_title2 = new QLabel(centralwidget);
        lbl_title2->setObjectName(QString::fromUtf8("lbl_title2"));
        lbl_title2->setGeometry(QRect(50, 450, 150, 40));
        QPalette palette15;
        palette15.setBrush(QPalette::Active, QPalette::WindowText, brush15);
        palette15.setBrush(QPalette::Active, QPalette::Text, brush);
        palette15.setBrush(QPalette::Active, QPalette::Base, brush);
        palette15.setBrush(QPalette::Active, QPalette::Window, brush2);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette15.setBrush(QPalette::Active, QPalette::PlaceholderText, brush18);
#endif
        palette15.setBrush(QPalette::Inactive, QPalette::WindowText, brush15);
        palette15.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette15.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette15.setBrush(QPalette::Inactive, QPalette::Window, brush2);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette15.setBrush(QPalette::Inactive, QPalette::PlaceholderText, brush18);
#endif
        palette15.setBrush(QPalette::Disabled, QPalette::WindowText, brush14);
        palette15.setBrush(QPalette::Disabled, QPalette::Text, brush14);
        palette15.setBrush(QPalette::Disabled, QPalette::Base, brush2);
        palette15.setBrush(QPalette::Disabled, QPalette::Window, brush2);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette15.setBrush(QPalette::Disabled, QPalette::PlaceholderText, brush16);
#endif
        lbl_title2->setPalette(palette15);
        lbl_title2->setFont(font);
        lbl_title2->setAutoFillBackground(true);
        lbl_title2->setAlignment(Qt::AlignCenter);
        lbl_background_input = new QLabel(centralwidget);
        lbl_background_input->setObjectName(QString::fromUtf8("lbl_background_input"));
        lbl_background_input->setGeometry(QRect(870, 0, 371, 951));
        QPalette palette16;
        palette16.setBrush(QPalette::Active, QPalette::Base, brush);
        palette16.setBrush(QPalette::Active, QPalette::Window, brush2);
        palette16.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette16.setBrush(QPalette::Inactive, QPalette::Window, brush2);
        palette16.setBrush(QPalette::Disabled, QPalette::Base, brush2);
        palette16.setBrush(QPalette::Disabled, QPalette::Window, brush2);
        lbl_background_input->setPalette(palette16);
        lbl_background_input->setAutoFillBackground(true);
        lbl_slider_text_2 = new QLabel(centralwidget);
        lbl_slider_text_2->setObjectName(QString::fromUtf8("lbl_slider_text_2"));
        lbl_slider_text_2->setGeometry(QRect(920, 190, 120, 40));
        QPalette palette17;
        palette17.setBrush(QPalette::Active, QPalette::WindowText, brush15);
        palette17.setBrush(QPalette::Inactive, QPalette::WindowText, brush15);
        palette17.setBrush(QPalette::Disabled, QPalette::WindowText, brush14);
        lbl_slider_text_2->setPalette(palette17);
        MainWindow->setCentralWidget(centralwidget);
        lbl_background_input->raise();
        btn_image->raise();
        lbl_image->raise();
        horizontalSlider->raise();
        lbl_slider_text->raise();
        lbl_slider_value->raise();
        radioButton_1->raise();
        radioButton_2->raise();
        radioButton_3->raise();
        radioButton_4->raise();
        radioButton_5->raise();
        radioButton_6->raise();
        btn_execute->raise();
        lbl_image_2->raise();
        lbl_title1->raise();
        lbl_title2->raise();
        lbl_slider_text_2->raise();

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Projet Harmonie", nullptr));
        btn_image->setText(QApplication::translate("MainWindow", "Choisir une Image", nullptr));
        lbl_image->setText(QString());
        lbl_slider_text->setText(QApplication::translate("MainWindow", "Angle Ouverture :", nullptr));
        lbl_slider_value->setText(QApplication::translate("MainWindow", "0\302\260", nullptr));
        radioButton_1->setText(QApplication::translate("MainWindow", "Compl\303\251mentaire", nullptr));
        radioButton_2->setText(QApplication::translate("MainWindow", "Compl\303\251mentaire Adjacente", nullptr));
        radioButton_3->setText(QApplication::translate("MainWindow", "T\303\251tradique", nullptr));
        radioButton_4->setText(QApplication::translate("MainWindow", "Triadique", nullptr));
        radioButton_5->setText(QApplication::translate("MainWindow", "Monochrome", nullptr));
        radioButton_6->setText(QApplication::translate("MainWindow", "Analogue", nullptr));
        btn_execute->setText(QApplication::translate("MainWindow", "Sauvegarder l'image", nullptr));
        lbl_image_2->setText(QString());
        lbl_title1->setText(QApplication::translate("MainWindow", "Image choisie", nullptr));
        lbl_title2->setText(QApplication::translate("MainWindow", "Pr\303\251visualisation", nullptr));
        lbl_background_input->setText(QString());
        lbl_slider_text_2->setText(QApplication::translate("MainWindow", "Harmonie :", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
